# csStandalone

A widget script that demonstrates the boundary detection and coherent Set calculation.

## How to use?

On your local machine simply run the script giving the keyword "Boundary" to inspect boundary detection or "Sets" to inspect clustering. E.G. on a UNIX based system:

`
python main.py Boundary
`

Note, that the files calc.py and plot.py have to be present in the same folder. You can find these in https://git.imp.fu-berlin.de/sfb1114-a08/wp21


