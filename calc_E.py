#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 15:18:01 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

from sys import argv, path
import numpy as np
path.append("../wp21/pyscripts/LIB")
import calc as cc
import data as dd
from datetime import datetime

# %% Get the data

if argv[0]=='':
     date_0 = datetime(2016,4,29,6)
     trapath = "../wp21/era5/traj/2016/"

else:
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    trapath = argv[1]
    print(date_0)

fname = "traj" + date_0.strftime('_%Y%m%d_%H') + ".npy"
trajs = np.load(trapath + fname)

for a in [0,1]:
    if a == 0:

        t_sel = np.arange(0,int(trajs.shape[1]/2)+1)
    else:
        t_sel = np.arange(int(trajs.shape[1]/2),trajs.shape[1])


    z_coord_type = "p"

    n = 1

    path = "./" + date_0.strftime("dat_%Y%m%d_%H") + "/" + str(n) + "/"
    if not exists(path):
        makedirs(path)

    print(n)

    Lon = trajs['lon'][::n]
    Lat = trajs['lat'][::n]
    P = trajs['p'][::n]
    U = trajs['U'][::n]
    V = trajs['V'][::n]
    Omg = trajs['OMEGA'][::n]
    T = trajs['T'][::n]

    # %% Initials

    H = cc.ptoh(P, 1013.25, 8.435) # standard atmosphere

    # k_p = cc.calc_k(U/1000, V/1000, Omg/100)
    k_p=15
    # k_h = cc.calc_k(U, V, cc.omg2w(Omg/100, T, P))

    D = list((list(), list(), list()))

    epsilon_opt = np.array([20000, 50000, 100000, 200000, 500000])
    bound_meth = "$\\alpha$"
    alpha = 1e-3

    for epsilon in epsilon_opt:

        print(epsilon)

        if z_coord_type=="p":
            x, y, z = cc.coord_trans(Lon, Lat, P, "None", 1,
                                     proj="stereo")

            if (bound_meth == "Concave"):
                boundscs, hullscs = dd.io_bounds(path + "stereop",# + "norm" ,
                                             "opt. $\\alpha$", x, y, z, None)
                E = dd.io_eigen((path + "spnopt" + str(t_sel[0]) +
                             str(t_sel[-1])),
                            Lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2],
                            epsilon, boundscs, 20, Lon, Lat, P, True, k_p, "p")

            elif (bound_meth == "Convex"):
                boundscs, hullscs = dd.io_bounds(path + "stereop",#  + "norm" ,
                                                 "Convex", x, y, z, None)
                E = dd.io_eigen((path + "spncvx" + str(t_sel[0]) +
                             str(t_sel[-1])),
                            Lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2],
                            epsilon, boundscs, 20, Lon, Lat, P, True, k_p, "p")

            elif (bound_meth == "$\\alpha$"):
                # for alpha in np.array([1e-4, 2e-4, 5e-4, 0.001, 0.002, 0.005, 0.01]):
                boundscs, hullscs = dd.io_bounds(path + "stereop", "$\\alpha$",
                                                 x, y, z, alpha)
                E = dd.io_eigen((path + "spnalp" + str(alpha) + str(t_sel[0]) +
                         str(t_sel[-1])),
                        Lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2],
                        epsilon, boundscs, 20, Lon, Lat, P, True, k_p, "p",
                        force_calc=True)

        else:
            x, y, z = cc.coord_trans(Lon, Lat, H, "None", 1,
                                     proj="stereo")

            if (bound_meth == "Concave"):
                boundscs, hullscs = dd.io_bounds(path + "stereoh",# + "norm" ,
                                             "opt. $\\alpha$", x, y, z, None)
                E = dd.io_eigen((path + "shnopt" + str(t_sel[0]) +
                             str(t_sel[-1])),
                            Lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2],
                            epsilon, boundscs, 20, Lon, Lat, H, True, k_h, "H")

            elif (bound_meth == "Convex"):
                boundscs, hullscs = dd.io_bounds(path + "stereoh",#  + "norm" ,
                                                 "Convex", x, y, z, None)
                E = dd.io_eigen((path + "shncvx" + str(t_sel[0]) +
                             str(t_sel[-1])),
                            Lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2],
                            epsilon, boundscs, 20, Lon, Lat, H, True, k_h, "H")

            elif (bound_meth == "$\\alpha$"):
                for alpha in np.array([1e-4, 2e-4, 5e-4, 0.001, 0.002, 0.005, 0.01]):
                    boundscs, hullscs = dd.io_bounds(path + "stereoh", "$\\alpha$",
                                                     x, y, z, alpha)
                    E = dd.io_eigen((path + "shnalp" + str(alpha) + str(t_sel[0]) +
                             str(t_sel[-1])),
                            Lon.shape[0], t_sel[0], t_sel[-1], D[0], D[1], D[2],
                            epsilon, boundscs, 20, Lon, Lat, H, True, k_h, "H")