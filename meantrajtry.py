#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 14:08:40 2023

@author: schoelleh96
"""

from sklearn.neighbors import KernelDensity
import numpy as np
from matplotlib import pyplot as plt
import plotly.graph_objects as go

import plotly.io as pio
pio.renderers.default='svg'


mean_x = np.mean(x, axis=0)
mean_y = np.mean(y, axis=0)
mean_z = np.mean(z, axis=0)

def compute_3d_kde(x, y, z, bandwidth=0.5):
    # Stack the data into shape (n_p * n_t, 3)
    data = np.vstack([x.ravel(), y.ravel(), z.ravel()]).T

    # Fit the KDE model
    kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(data)
    return kde

def plot_density_isosurface_plotly(kde, mean_x, mean_y, mean_z, grid_size=30):
    # Define a grid
    X, Y, Z = np.meshgrid(
        np.linspace(mean_x.min(), mean_x.max(), grid_size),
        np.linspace(mean_y.min(), mean_y.max(), grid_size),
        np.linspace(mean_z.min(), mean_z.max(), grid_size),
    )
    grid_points = np.c_[X.ravel(), Y.ravel(), Z.ravel()]

    # Evaluate the density on the grid
    density = np.exp(kde.score_samples(grid_points))
    density = density.reshape((grid_size, grid_size, grid_size))

    # Define the level for the isosurface (68.2%)
    level = np.percentile(density, 68.2)

    # Create a plotly figure
    fig = go.Figure()

    # Add the isosurface
    fig.add_trace(go.Isosurface(
        x=X.flatten(),
        y=Y.flatten(),
        z=Z.flatten(),
        value=density.flatten(),
        isomin=level,
        isomax=density.max(),
        opacity=0.5,
        surface_count=1,
        colorscale="Viridis",
    ))

    # Add the mean trajectory
    fig.add_trace(go.Scatter3d(
        x=mean_x,
        y=mean_y,
        z=mean_z,
        mode='lines',
        line=dict(color='red', width=5)
    ))

    fig.show()

kde = compute_3d_kde(x, y, z)
plot_density_isosurface_plotly(kde, mean_x, mean_y, mean_z)
