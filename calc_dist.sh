#!/bin/bash

#SBATCH --array=1-24                                                                                     

year=2016
TRAPATH=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/wp21/era5/traj
CSPATH=/net/scratch/schoelleh96/WP2/WP2.1/LAGRANTO/csstandalone

cd ${TRAPATH}/${year}/
trafiles=`ls -v traj*`
CurrTrajF=`echo $trafiles | cut --delimiter " " --fields $SLURM_ARRAY_TASK_ID`

filename=${CurrTrajF##*/}
Y1=${filename:5:4}
M1=${filename:9:2}
D1=${filename:11:2}
H1=${filename:14:2}

python ${CSPATH}/calc_dist.py ${TRAPATH}/${year}/  ${Y1} ${M1} ${D1} ${H1}
