#!/bin/bash

#SBATCH --array=0-3

TRADIR=${1}

python calc_bound.py ${SLURM_ARRAY_TASK_ID} ${TRADIR} ${2} ${3} ${4} ${5}  
