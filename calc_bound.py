#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 16:39:28 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

from sys import argv, path, stdout
import numpy as np
path.append("../wp21/pyscripts/LIB")
import calc as cc
import data as dd
from datetime import datetime
import itertools

# %% Get the data

opts = list(itertools.product([1,10], ["x, y, p (stereo)", "Lon, Lat, p"]))

norm = False

if argv[0]=='':
     n = 1
     coord = "x, y, p (stereo)"
     date_0 = datetime(2016,5,1,6)
     trapath = "../wp21/era5/traj/2016/"

else:
    n = opts[int(argv[1])][0]
    coord = opts[int(argv[1])][1]
    date_0 = datetime(int(argv[3]),int(argv[4]),int(argv[5]),int(argv[6]))
    trapath = argv[2]

fname = "traj" + date_0.strftime('_%Y%m%d_%H') + ".npy"
trajs = np.load(trapath + fname)

# only every nth trajectory
path = "./" + date_0.strftime("dat_%Y%m%d_%H") + "/" + str(n) + "/"
if not exists(path):
    makedirs(path)
print(n, flush=True)
# Full variables for boundary calculation
Lon = trajs['lon'][::n]
Lat = trajs['lat'][::n]
P = trajs['p'][::n] # is in hPa

# %% Initials

H = cc.ptoh(P, 1013.25, 8.435) # standard atmosphere

# %% loop over combinations

print(norm)

print(coord)
if coord == "Lon, Lat, p":
    x, y, z = Lon, Lat, P
    d_path = path + "lonlatp"
    if (not norm):
        z = z * cc.calc_k(trajs['U']/1000, trajs['V']/1000,
                          trajs['OMEGA']/100)
elif coord == "Lon, Lat, h":
    x, y = Lon, Lat
    z = cc.ptoh(P, 1013.25, 8.435)
    d_path = path + "lonlath"
    if (not norm):
        z = z * cc.calc_k(trajs['U'], trajs['V'],
                          cc.omg2w(trajs['OMEGA']/100,
                                   trajs['T'], trajs['p']))
elif coord == "x, y, p (stereo)":
    x, y, z = cc.coord_trans(Lon, Lat, P, "None", 1, proj="stereo")
    d_path = path + "stereop"
    if (not norm):
        z = z * 15 #cc.calc_k(trajs['U']/1000, trajs['V']/1000,
                          # trajs['OMEGA']/100)
elif coord == "x, y, h (stereo)":
    x, y, z = cc.coord_trans(Lon, Lat, P, "std_atm", 1, proj="stereo")
    d_path = path + "stereoh"
    if (not norm):
        z = z * cc.calc_k(trajs['U'], trajs['V'],
                          cc.omg2w(trajs['OMEGA']/100,
                                   trajs['T'], trajs['p']))
elif coord == "x, y, z (3d)":
    x, y, z = cc.coord_trans(Lon, Lat, P, "std_atm", -1)
    d_path = path + "xyz"

# Normalization
if norm:
    x, y, z = cc.norm(x), cc.norm(y), cc.norm(z)
    d_path = d_path + "norm"

for meth in ["opt. $\\alpha$", "Convex", "$\\alpha$"]:
    print(meth)
    if meth == "$\\alpha$":
        for alpha in [1e-5, 2e-5, 5e-5, 1e-4, 2e-4, 5e-4, 1e-3, 2e-3, 5e-3,
                      1e-2, 2e-2, 5e-2]:
            # Boundary calculation
            bounds, hulls = dd.io_bounds(d_path, meth, x, y,
                                 z, alpha)
    else:
        bounds, hulls = dd.io_bounds(d_path, meth, x, y,
                                     z, alpha)
    stdout.flush()