#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 15:42:22 2023

@author: schoelleh96
"""

# %% library imports

# This will move the console to the right working directory.
from os.path import  dirname, abspath, exists
from os import chdir, makedirs
chdir(dirname(abspath(__file__)))

from sys import argv, path
import numpy as np
path.append("../wp21/pyscripts/LIB")
import calc as cc
import data as dd
from datetime import datetime

# %% Get the data

# opts = list(itertools.product([1,10], ["H"]))
z_coord_type = "p"

if argv[0]=='':
     date_0 = datetime(2016,5,1,6)
     trapath = "../wp21/era5/traj/2016/"

else:
    date_0 = datetime(int(argv[2]),int(argv[3]),int(argv[4]),int(argv[5]))
    trapath = argv[1]

fname = "traj" + date_0.strftime('_%Y%m%d_%H') + ".npy"
trajs = np.load(trapath + fname)
t_sel = np.arange(0,trajs.shape[1])

e = 1e5

for n in [1]:

    # only every nth trajectory
    print(n)

    path = "./dat_variablekp/" + date_0.strftime("dat_%Y%m%d_%H") + "/" + str(n) + "/"
    if not exists(path):
        makedirs(path)

    Lon = trajs['lon'][::n, t_sel]
    Lat = trajs['lat'][::n, t_sel]
    P = trajs['p'][::n, t_sel]
    U = trajs['U'][::n, t_sel]
    V = trajs['V'][::n, t_sel]
    Omg = trajs['OMEGA'][::n, t_sel]
    T = trajs['T'][::n, t_sel]

    # %% Initials

    H = cc.ptoh(P, 1013.25, 8.435) # standard atmosphere

    # k_p = cc.calc_k(U/1000, V/1000, Omg/100)
    k_p = 15
    k_h = cc.calc_k(U, V, cc.omg2w(Omg/100, T, P))

    print(z_coord_type)
    if z_coord_type=="p":
        D = dd.io_dist(path, e, Lon, Lat, P, True, k_p,
                       z_coord_type, t_sel, returnD=False, force_calc=True)
    else:
        D = dd.io_dist(path, e, Lon, Lat, H, True, k_h,
                       z_coord_type, t_sel, returnD=False, force_calc=True)
